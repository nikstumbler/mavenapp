
import sun.misc.BASE64Decoder;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.spec.*;
import java.util.Base64;

/**
 * Created by e070769 on 4/12/17.
 *
 * keytool -genkeypair -alias acmebank -storepass masterpass -keypass masterpass -keyalg RSA -keystore KeyStore.jks
 *
 * keytool -genkeypair -alias citibank -storepass masterpass -keypass masterpassforciti -keyalg RSA -keystore KeyStore.jks
 *
 */
public class SignAndVerify {

    private static String walletId = "citibank";
    private static String keyStorePassword = "masterpass";
    private static String keyPasswordStr = "masterpassforciti";

    private static PrivateKey privateKey;

    private static PublicKey publicKey;

    public static void main(String[] args) {

        String payload_signed = "{\"walletId\":\"acmebank\",\"locale\":\"en_US\",\"redirectURL\":\"testURL\"}";

        String payload_verify = "{\"walletId\":\"acmebank\",\"locale\":\"en_US\",\"redirectURL\":\"testURL\"}";

//        generateKeys();

//        generateKeysFromDER();

//        getKeyPairFromKeyStore();

        String signature = signData(payload_signed,privateKey);

        System.out.println(signature.trim());

        boolean isVerified = verifySignature(payload_verify,signature,publicKey);

//        System.out.println("Verification Result : " + isVerified);

    }

    public static KeyPair getKeyPairFromKeyStore() {

        KeyStore.PrivateKeyEntry privateKeyEntry = null;
        java.security.cert.Certificate cert = null;
        try {

            File f = new File("src/KeyStore.jks");
            FileInputStream fis = new FileInputStream(f);
            DataInputStream dis = new DataInputStream(fis);
            InputStream ins = dis;

            KeyStore keyStore = KeyStore.getInstance("JCEKS");
            keyStore.load(ins, keyStorePassword.toCharArray());   //Keystore password
            KeyStore.PasswordProtection keyPassword =       //Key password
                    new KeyStore.PasswordProtection(keyPasswordStr.toCharArray());

            privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(walletId, keyPassword);

            cert = keyStore.getCertificate(walletId);

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        }
        publicKey = cert.getPublicKey();
        privateKey = privateKeyEntry.getPrivateKey();

        return new KeyPair(publicKey, privateKey);
    }

    public static void generateKeysFromDER() {

//        readPrivateKey("src/private_key.der");

        readPrivateKeyPEM("private_key.pem");

        readPublicKey("src/public_key.der");

    }

    public static void generateKeysFromPEM(String filename) {

        readPrivateKeyPEM("private_key.pem");
//        readPublicKey("src/public_key.pem");

    }

    public static void readPrivateKeyPEM(String filename) {
        KeyFactory kf;
        try {

            File f = new File(filename);
            FileInputStream fis = new FileInputStream(f);
            DataInputStream dis = new DataInputStream(fis);
            byte[] keyBytes = new byte[(int) f.length()];
            keyBytes = new String("MIIEpQIBAAKCAQEAz+nmGcsOCVtRSwOStASthKyagMLR8ihcSDDpc2InX6jQbsrXxHN22l9dzKnIfnU/0b11Qe5x9xDEvZRpJ25WMGtWIy2TdHSO43B4krsSSWHohvexHqoBKd38nwjOYy/fTwMQMpT+CzjLuGqbL44X0KSbTxgitzos6evqLeN/x85WKQTz2evUmwbYqpO+VEOg/BQoEgQW2zgsL9svkINDi1/zTtp4NvhbBqbjY7YxNe1BLbdcaGkto3APpKaZSyaiV9u+3HWCVGcvisjf5mk0irSWjJyLUg7b65mq11WXpYWFu195Rsa1NQKzof1iCFHnaspu13bDGmW0qwOECAe8YQIDAQABAoIBAQCOkTm6WBWA7wA4avPOwyJkxqKPRogGAAz/Z6K8/wI2wyBhyG/aAr7uENTWAVo89JNVdA+SrTmyi+oMgVb1teAeBYwkRR1GjqJobPgQzXxFm4CCBBiNCVAQ8GpIdxZmxy3eFSCnPqlQ8ponE9rj6hCEfksNGmS7tYWUxbNn0gcN7z5+fLIWGsXrfbCQKTnQYbQCxqK0NJUy4f1UrQDGNjjT0Q2Jx/MoOcLP2Aw4qeUWMl4uuvPNkqMldx1jb9K0zcnUv4QFPp67dLGn+vB5AQo4vjOEu1aS7HWA3cE1DCOLbhnX7597YWDcm1IJCejRXE1YYcKX7Wb3gXoIE9fhblotAoGBAPrQmEqEyhrdcbZ41jk/0ZpPARBrrypdj4Yu/P+0FOJrN2olHNDUEvH79L459I6jJ9xJ6mbvlVK1vUFbsTu6nxB/prRboCWmDpHxv9Rr8aiMfzt9dMjcICMYgZvQHXJl2NauNKlOXRmdzm4cNxKIQKpH3IAeRvs/aLUI3Zr/EZbnAoGBANQ2QU6WMBYpLq93JLD5eBFk9R6pxVaiFmjXPapQbmhcLyivqlZFcfOBf99I0Es6BtQzeP/Kj13OvGNp1o08ZjIB5hH6g93PtufAn5YrHDqYacwWBrev2BTJ8CRiy8rnJrcMnKyCSdSozMtvuqULc4qMq1hP1WMw7jZ6Kg7DGNF3AoGBAIr66VhpioAmcutUvAgPF/s0ifdqZC7Kl0wYX3Lle6kXFibIF9aTnUffgHekL6KWP/EuOCf/3DNvp0Y/I89gEuIWie5o0y3pfStb1RtFESx/ZcU2OG9QorIv0ynewWJxkx0qfN3QIYCfVBLpqY6oV4TDuyh3Hof8U+300q8fZcTDAoGBAJ4UCFP1Fr77UHaOpvtZ6jHy5ZFA3+pn9Y+Ffr8HCSeeO9bv/FAljPvaZJm8kMTo5bte2GyaxG8ZmEXpylQNQQdRWMPdvCsYFuTOHVLsrCuN64mGgtyEFc9umf9QxQRRagqDT5ZFu5+5WkzU3tEhFaGIpf60AX3RLjHQkNHMTQmDAoGAe0Nt55LhSPj5yDDHnwbQBXMFbm6LCnRAWYj++NKlUrXOJF1w/SKOwedsNpeE8GXiuS6fXblT9r66i0XfXm8LfseUFW+9SgMwwO2kJTg8rIqQSbFtD18HE7bW4x28AIJ4HKxgFomCb4qw91DWqfZNzKmoJDMxZ57JytovXyulC00=").getBytes();
            dis.readFully(keyBytes);
//            dis.close();

            String private_key_pem = new String(keyBytes);
            private_key_pem = private_key_pem.replace("-----BEGIN RSA PRIVATE KEY-----\n","");
            private_key_pem = private_key_pem.replace("-----END RSA PRIVATE KEY-----","");
            private_key_pem = private_key_pem.trim();

//            Base64 b64 = new Base64();
//            byte [] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(private_key_pem);
//            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(decoded);

            byte[] encoded = DatatypeConverter.parseBase64Binary(private_key_pem);
            KeySpec spec = readKeysFromPEM(encoded);
//            getRSAKeySpec(encoded);

//            kf = KeyFactory.getInstance("RSA");
//            privateKey = kf.generatePrivate(spec);

        } catch (IOException e) {
            e.printStackTrace();
        }/* catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }*/

    }

    private static RSAPrivateCrtKeySpec readKeysFromPEM(byte[] keyBytes) throws IOException  {

//        PrivateKeyReader

/*        DerParser parser = new DerParser(keyBytes);

        Asn1Object sequence = parser.read();
        if (sequence.getType() != DerParser.SEQUENCE)
            throw new IOException("Invalid DER: not a sequence"); //$NON-NLS-1$

        // Parse inside the sequence
        parser = sequence.getParser();

        parser.read(); // Skip version
        BigInteger modulus = parser.read().getInteger();
        BigInteger publicExp = parser.read().getInteger();
        BigInteger privateExp = parser.read().getInteger();
        BigInteger prime1 = parser.read().getInteger();
        BigInteger prime2 = parser.read().getInteger();
        BigInteger exp1 = parser.read().getInteger();
        BigInteger exp2 = parser.read().getInteger();
        BigInteger crtCoef = parser.read().getInteger();

        RSAPrivateCrtKeySpec keySpec = new RSAPrivateCrtKeySpec(
                modulus, publicExp, privateExp, prime1, prime2,
                exp1, exp2, crtCoef);

        return keySpec;*/

        return null;
    }

/*
    private static RSAPrivateCrtKeySpec getRSAKeySpec(byte[] keyBytes) throws IOException  {

        DerParser parser = new DerParser(keyBytes);

        Asn1Object sequence = parser.read();
        if (sequence.getType() != DerParser.SEQUENCE)
            throw new IOException("Invalid DER: not a sequence"); //$NON-NLS-1$

        // Parse inside the sequence
        parser = sequence.getParser();

        parser.read(); // Skip version
        BigInteger modulus = parser.read().getInteger();
        BigInteger publicExp = parser.read().getInteger();
        BigInteger privateExp = parser.read().getInteger();
        BigInteger prime1 = parser.read().getInteger();
        BigInteger prime2 = parser.read().getInteger();
        BigInteger exp1 = parser.read().getInteger();
        BigInteger exp2 = parser.read().getInteger();
        BigInteger crtCoef = parser.read().getInteger();

        RSAPrivateCrtKeySpec keySpec = new RSAPrivateCrtKeySpec(
                modulus, publicExp, privateExp, prime1, prime2,
                exp1, exp2, crtCoef);

        return keySpec;
    }
*/


    public static void readPrivateKey(String filename) {
        KeyFactory kf;
        try {

            File f = new File(filename);
            FileInputStream fis = new FileInputStream(f);
            DataInputStream dis = new DataInputStream(fis);
            byte[] keyBytes = new byte[(int) f.length()];
            dis.readFully(keyBytes);
            dis.close();

            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
            kf = KeyFactory.getInstance("RSA");
            privateKey = kf.generatePrivate(spec);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    public static void readPublicKey(String filename) {
        KeyFactory kf;
        try {

            File f = new File(filename);
            FileInputStream fis = new FileInputStream(f);
            DataInputStream dis = new DataInputStream(fis);
            byte[] keyBytes = new byte[(int) f.length()];
            dis.readFully(keyBytes);
            dis.close();

            X509EncodedKeySpec xspec = new X509EncodedKeySpec(keyBytes);
            kf = KeyFactory.getInstance("RSA");
            publicKey = kf.generatePublic(xspec);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    public static void generateKeys() {
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");

            SecureRandom secureRandom = new SecureRandom();
            secureRandom.setSeed(walletId.getBytes());

            generator.initialize(1024,secureRandom);
            KeyPair keyPair = generator.generateKeyPair();

            privateKey = keyPair.getPrivate();
            publicKey = keyPair.getPublic();

            System.out.println("PrivateKey : " + privateKey);
            System.out.println("PublicKey  : " + publicKey);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static String signData(String payload, PrivateKey privateKey){

        byte[] signature = new byte[0];
        try {

            Signature privateSignature = Signature.getInstance("SHA1withRSA");
//            Signature privateSignature = Signature.getInstance("SHA1withDSA");

            privateSignature.initSign(privateKey);
            privateSignature.update(payload.getBytes());

            signature = privateSignature.sign();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }

        return Base64.getEncoder().encodeToString(signature);
    }

    public static Boolean verifySignature(String payload, String signature, PublicKey publicKey) {
//        X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec( DatatypeConverter.parseBase64Binary(publicKey) );

        boolean result = false;
        try {
            Signature signetcheck = Signature.getInstance("SHA1withRSA");
            signetcheck.initVerify(publicKey);
            signetcheck.update(payload.getBytes());



            if (signetcheck.verify( Base64.getDecoder().decode(signature) )) {
                System.out.println("Vinfo=" + payload);
                result= true;
            } else {
                System.out.println("Error");
                result= false;
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return result;
    }
}

/*

class DerParser {

    // Classes
    public final static int UNIVERSAL = 0x00;
    public final static int APPLICATION = 0x40;
    public final static int CONTEXT = 0x80;
    public final static int PRIVATE = 0xC0;

    // Constructed Flag
    public final static int CONSTRUCTED = 0x20;

    // Tag and data types
    public final static int ANY = 0x00;
    public final static int BOOLEAN = 0x01;
    public final static int INTEGER = 0x02;
    public final static int BIT_STRING = 0x03;
    public final static int OCTET_STRING = 0x04;
    public final static int NULL = 0x05;
    public final static int OBJECT_IDENTIFIER = 0x06;
    public final static int REAL = 0x09;
    public final static int ENUMERATED = 0x0a;
    public final static int RELATIVE_OID = 0x0d;

    public final static int SEQUENCE = 0x10;
    public final static int SET = 0x11;

    public final static int NUMERIC_STRING = 0x12;
    public final static int PRINTABLE_STRING = 0x13;
    public final static int T61_STRING = 0x14;
    public final static int VIDEOTEX_STRING = 0x15;
    public final static int IA5_STRING = 0x16;
    public final static int GRAPHIC_STRING = 0x19;
    public final static int ISO646_STRING = 0x1A;
    public final static int GENERAL_STRING = 0x1B;

    public final static int UTF8_STRING = 0x0C;
    public final static int UNIVERSAL_STRING = 0x1C;
    public final static int BMP_STRING = 0x1E;

    public final static int UTC_TIME = 0x17;
    public final static int GENERALIZED_TIME = 0x18;

    protected InputStream in;

    */
/**
     * Create a new DER decoder from an input stream.
     *
     * @param in
     *            The DER encoded stream
     *//*

    public DerParser(InputStream in) throws IOException {
        this.in = in;
    }

    */
/**
     * Create a new DER decoder from a byte array.
     *
     * @throws IOException
     *//*

    public DerParser(byte[] bytes) throws IOException {
        this(new ByteArrayInputStream(bytes));
    }

    */
/**
     * Read next object. If it's constructed, the value holds
     * encoded content and it should be parsed by a new
     * parser from <code>Asn1Object.getParser</code>.
     *
     * @return A object
     * @throws IOException
     *//*

    public Asn1Object read() throws IOException {
        int tag = in.read();

        if (tag == -1)
            throw new IOException("Invalid DER: stream too short, missing tag"); //$NON-NLS-1$

        int length = getLength();

        byte[] value = new byte[length];
        int n = in.read(value);
        if (n < length)
            throw new IOException("Invalid DER: stream too short, missing value"); //$NON-NLS-1$

        Asn1Object o = new Asn1Object(tag, length, value);

        return o;
    }

    */
/**
     * Decode the length of the field. Can only support length
     * encoding up to 4 octets.
     *
     * <p/>In BER/DER encoding, length can be encoded in 2 forms,
     * <ul>
     * <li>Short form. One octet. Bit 8 has value "0" and bits 7-1
     * give the length.
     * <li>Long form. Two to 127 octets (only 4 is supported here).
     * Bit 8 of first octet has value "1" and bits 7-1 give the
     * number of additional length octets. Second and following
     * octets give the length, base 256, most significant digit first.
     * </ul>
     * @return The length as integer
     * @throws IOException
     *//*

    private int getLength() throws IOException {

        int i = in.read();
        if (i == -1)
            throw new IOException("Invalid DER: length missing"); //$NON-NLS-1$

        // A single byte short length
        if ((i & ~0x7F) == 0)
            return i;

        int num = i & 0x7F;

        // We can't handle length longer than 4 bytes
        if ( i >= 0xFF || num > 4)
            throw new IOException("Invalid DER: length field too big (" //$NON-NLS-1$
                    + i + ")"); //$NON-NLS-1$

        byte[] bytes = new byte[num];
        int n = in.read(bytes);
        if (n < num)
            throw new IOException("Invalid DER: length too short"); //$NON-NLS-1$

        return new BigInteger(1, bytes).intValue();
    }

}


*/
/**
 * An ASN.1 TLV. The object is not parsed. It can
 * only handle integers and strings.
 *
 * @author zhang
 *
 *//*

class Asn1Object {

    protected final int type;
    protected final int length;
    protected final byte[] value;
    protected final int tag;

    */
/**
     * Construct a ASN.1 TLV. The TLV could be either a
     * constructed or primitive entity.
     *
     * <p/>The first byte in DER encoding is made of following fields,
     * <pre>
     *-------------------------------------------------
     *|Bit 8|Bit 7|Bit 6|Bit 5|Bit 4|Bit 3|Bit 2|Bit 1|
     *-------------------------------------------------
     *|  Class    | CF  |     +      Type             |
     *-------------------------------------------------
     * </pre>
     * <ul>
     * <li>Class: Universal, Application, Context or Private
     * <li>CF: Constructed flag. If 1, the field is constructed.
     * <li>Type: This is actually called tag in ASN.1. It
     * indicates data type (Integer, String) or a construct
     * (sequence, choice, set).
     * </ul>
     *
     * @param tag Tag or Identifier
     * @param length Length of the field
     * @param value Encoded octet string for the field.
     *//*

    public Asn1Object(int tag, int length, byte[] value) {
        this.tag = tag;
        this.type = tag & 0x1F;
        this.length = length;
        this.value = value;
    }

    public int getType() {
        return type;
    }

    public int getLength() {
        return length;
    }

    public byte[] getValue() {
        return value;
    }

    public boolean isConstructed() {
        return  (tag & DerParser.CONSTRUCTED) == DerParser.CONSTRUCTED;
    }

    */
/**
     * For constructed field, return a parser for its content.
     *
     * @return A parser for the construct.
     * @throws IOException
     *//*

    public DerParser getParser() throws IOException {
        if (!isConstructed())
            throw new IOException("Invalid DER: can't parse primitive entity"); //$NON-NLS-1$

        return new DerParser(value);
    }

    */
/**
     * Get the value as integer
     *
     * @return BigInteger
     * @throws IOException
     *//*

    public BigInteger getInteger() throws IOException {
        if (type != DerParser.INTEGER)
            throw new IOException("Invalid DER: object is not integer"); //$NON-NLS-1$

        return new BigInteger(value);
    }

    */
/**
     * Get value as string. Most strings are treated
     * as Latin-1.
     *
     * @return Java string
     * @throws IOException
     *//*

    public String getString() throws IOException {

        String encoding;

        switch (type) {

            // Not all are Latin-1 but it's the closest thing
            case DerParser.NUMERIC_STRING:
            case DerParser.PRINTABLE_STRING:
            case DerParser.VIDEOTEX_STRING:
            case DerParser.IA5_STRING:
            case DerParser.GRAPHIC_STRING:
            case DerParser.ISO646_STRING:
            case DerParser.GENERAL_STRING:
                encoding = "ISO-8859-1"; //$NON-NLS-1$
                break;

            case DerParser.BMP_STRING:
                encoding = "UTF-16BE"; //$NON-NLS-1$
                break;

            case DerParser.UTF8_STRING:
                encoding = "UTF-8"; //$NON-NLS-1$
                break;

            case DerParser.UNIVERSAL_STRING:
                throw new IOException("Invalid DER: can't handle UCS-4 string"); //$NON-NLS-1$

            default:
                throw new IOException("Invalid DER: object is not a string"); //$NON-NLS-1$
        }

        return new String(value, encoding);
    }
}
*/
