import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by e070769 on 4/19/17.
 */
public class TimestampWorkaround {

    public static void main(String[] args) {

        int minutes = 2;

        Date date = new Date();

        Long currTimeInLong = date.getTime();

        Timestamp timestamp = new Timestamp(currTimeInLong);

        String longStr = currTimeInLong.toString();

//        String currentTimestamp = timestamp.toString();

        System.out.println(date +  " : " + date.getTime());
        System.out.println(timestamp +  " : " + timestamp.getTime());
        System.out.println(currTimeInLong + " : " + currTimeInLong.toString());

//        SimpleDateFormat sdf = new SimpleDateFormat();
//        sdf.format()

        System.out.println("----- ---- ----- ----- -----");

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(Long.parseLong( longStr ) );
        cal.add(Calendar.MINUTE,minutes);
        java.sql.Timestamp ts_new_date_ws = new java.sql.Timestamp(cal.getTime().getTime());

        System.out.println("ts_new_date_ws" + " : " + ts_new_date_ws);
        System.out.println("ts_new_date_ws" + " : " + ts_new_date_ws.getTime());
    }

}
